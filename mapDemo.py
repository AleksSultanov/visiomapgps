""" VisioMapDemo
    version="1.0.0"
    Генерация иконок на карту. Демо режим 

   Результат в каталоге mapDemo, карта mapDemoIconColor.html
   
"""

import os
import folium
from folium.plugins import MousePosition
from folium import plugins

icons =["car", "truck", "sort-up", "male", "fire", "compass",
     "play-circle", "stop-circle", "fighter-jet", "train", "subway", "motorcycle", "bicycle",
     "bus", "map-marker", "plane", "ship", "camera", "camera-retro", 
     "cloud", "bolt", "umbrella"]

colors = ['orange', 'darkpurple', 'darkblue', 'lightblue', 'darkgreen', 'red', 
     'lightgreen', 'lightred', 'lightgray', 'cadetblue', 'green', 'purple',
      'beige', 'white', 'gray', 'blue', 'black', 'darkred', 'pink']     

mapFile = "mapDemoIconColor.html"
MapDirSave = 'mapDemo'

_lon = 41.65
_lat = 70.1
_zoom = 11
_map = folium.Map(location=[_lon, _lat],
                  tiles='openstreetmap', 
                   zoom_start=_zoom)
folium.TileLayer('OpenStreetMap', name="OpenStreet Map").add_to(_map)
folium.TileLayer('Stamen Terrain', name="Stamen Terrain").add_to(_map) 
folium.TileLayer('Stamen Toner', name="Stamen Toner").add_to(_map) 
folium.TileLayer('Stamen Watercolor', name="Stamen Watercolor").add_to(_map) 
folium.TileLayer('Cartodb Positron', name="Cartodb Positron").add_to(_map) 
folium.TileLayer('Cartodb dark_matter', name="Cartodb dark_matter").add_to(_map) 

folium.TileLayer("https://server.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer/tile/{z}/{y}/{x}",
    attr="Tiles &copy; Esri &mdash; National Geographic, Esri, DeLorme, NAVTEQ, UNEP-WCMC, USGS, NASA, ESA, METI, NRCAN, GEBCO, NOAA, iPC"
    ,name="Nat Geo Map").add_to(_map)


fg = folium.FeatureGroup(name='Иконки без заливки', show=False).add_to(_map)
_c = 0
for _color in colors:
    _c += 0.01
    _i = 0
    for _icon in icons:
        _i += 0.01
        folium.Marker(location=[_lon +_c, _lat + _i], 
                    popup = f"<b>Цвет</b> {_color} <br> <b>Иконка</b> {_icon}", 
                    icon=folium.Icon(color=_color,
                                icon_color=_color,
                                icon=_icon,
                                prefix="fa",
                                icon_size = [8,8],
                                shadow_size = [5,5]
                                )
                    ).add_to(_map).add_to(fg)                      

for _icon in icons:
    fg = folium.FeatureGroup(name=_icon, show=False).add_to(_map)
    _c = 0
    for _color in colors:
        _c += 0.01
        _i = 0
        for _icon_color in colors:
            _i += 0.01
            folium.Marker(location=[_lon +_c, _lat + _i], 
                    popup = f"<b>Иконка</b> {_icon} <br> <b>Цвет</b> {_color} <br> <b>Цвет иконки</b> {_icon_color}", 
                    icon=folium.Icon(color=_color,
                                icon_color=_icon_color,
                                icon=_icon,
                                prefix="fa"
                                )
                    ).add_to(_map).add_to(fg) 

plugins.Fullscreen(position="topright",
    title="Просмотр на полном экране",
    title_cancel="Нажми для выхода",
    force_separate_button=True,
    ).add_to(_map)
folium.LayerControl(collapsed=True).add_to(_map)  
MousePosition().add_to(_map)
#сохраняем карту 
if not os.path.exists(MapDirSave):
    os.mkdir(MapDirSave)
mapFile = os.path.join(MapDirSave,mapFile)      
_map.save(mapFile)

