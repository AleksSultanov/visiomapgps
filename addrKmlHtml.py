""" AddrKmlHtml
    version="1.0.0"

Модуль для конвертации gps меток из kml в html

Запуск с параметрами:
  -d -директория с файлами в формате kml
"""
import os
import sys
import xml.etree.ElementTree as ET 

C_ICON_W = 'width="16"'

def pathparse(root_tag, elem, list):
    if len(list) == 0:
        return None
    for subelem in elem:
        subTag = subelem.tag.replace(root_tag,'')
        if subTag == list[0]:
            if len(list[1::]) > 0 :
                return pathparse(root_tag,subelem,list[1::])
            else:  
                return subelem.text
    return None               

def folderparse(root_tag, elem, fdict):
    _name = pathparse(root_tag, elem, ['name'])
    fdict[_name] = {}
    for subelem in elem:
        subTag = subelem.tag.replace(root_tag,'')
        if subTag == 'Folder':
            folderparse(root_tag, subelem, fdict)
        if subTag == 'Placemark':
            placename = pathparse(root_tag,subelem,['name'])
            coordinates = pathparse(root_tag,subelem,['Point','coordinates'])
            if coordinates == None:
                longitude = None
                latitude = None
                altitude = 0
            else:    
                longitude = coordinates.split(',')[0]
                latitude = coordinates.split(',')[1]
                altitude = coordinates.split(',')[2]
            styleUrl = pathparse(root_tag,subelem,['styleUrl'])
            fdict[_name][placename] = {"longitude":longitude,
                                      "latitude":latitude,
                                      "altitude":altitude,
                                      "styleUrl":styleUrl,
                                      }

def mapsUrl(g_lat, g_lon):
    if g_lat == None or g_lon == None:
        return ''
    C_TARGET = 'target = "_blank"'
    C_SCALE = 16
    urlGoogle  = f'<a href = "http://www.google.com/maps?q={g_lat},{g_lon}" {C_TARGET}><img src="http://www.google.com/favicon.ico" {C_ICON_W} ></a>'
    urlOstreet = f'<a href = "https://www.openstreetmap.org/#map={C_SCALE}/{g_lat}/{g_lon}" {C_TARGET}><img src="https://openstreetmap.org/favicon.ico" {C_ICON_W} ></a>'
    urlOSMAND  = f'<a href = "https://osmand.net/go?lat={g_lat}&lon={g_lon}&z={C_SCALE}" {C_TARGET}><img src="https://osmand.net/images/favicons/favicon.ico" {C_ICON_W} ></a>'
    urlYandex  = f'<a href = "https://yandex.ru/maps?pt={g_lon},{g_lat}&z={C_SCALE}" {C_TARGET}><img src="https://yandex.ru/maps/favicon.ico" {C_ICON_W} ></a>'
    urlWindy   = f'<a href = "https://windy.com/{g_lat}/{g_lon}&z={C_SCALE}" {C_TARGET}><img src="https://windy.com/favicon.ico" {C_ICON_W} ></a>'
   
    url =f'{urlGoogle} {urlOSMAND} {urlYandex}'
    return url


startDir  = '.'
expDir    = 'adrHtml'

if '-d' in sys.argv:   
    diridx = sys.argv.index('-d')+1
    if sys.argv[diridx] != None and os.path.isdir(sys.argv[diridx]):
        startDir  = sys.argv[diridx]

for _file in os.listdir(startDir):
    kml_file = os.path.join(startDir,_file)
    if kml_file[-3::].upper() == 'KML': 
        styledict = {}
        styleMapdict = {}
        folderDict = {}
        htmlbody  = '' 
        exp_file = os.path.join(expDir,_file[:-3]+'html')
        root_node = ET.parse(kml_file).getroot()
        root_tag = root_node.tag[:-3]

        for elem in root_node:
            for subelem in elem:
                subTag = subelem.tag.replace(root_tag,'')
                if subTag == 'Style':
                    _value = pathparse(root_tag,subelem,['IconStyle','Icon','href'])
                    styledict[subelem.attrib['id']] = _value
                if subTag == 'StyleMap':
                    _value = pathparse(root_tag,subelem,['Pair','styleUrl'])
                    styleMapdict[subelem.attrib['id']] = _value
                if subTag == 'Folder':
                    folderparse(root_tag, subelem, folderDict)    

        for _folder in folderDict:
           htmlbody  = f"{htmlbody}<br><details open> <summary>{_folder}</summary><ul type=disk>"
           for _address in folderDict[_folder]:
               lon = folderDict[_folder][_address]["longitude"]
               lat = folderDict[_folder][_address]["latitude"]
               url = mapsUrl(lat,lon)
               
               style = folderDict[_folder][_address]["styleUrl"][1:]
               styleUrl = styleMapdict[style][1:]
               imageurel = styledict[styleUrl]

               htmlbody  = f'{htmlbody}<li><img src="{imageurel}" {C_ICON_W} > {_address} {url}'	
           htmlbody  = f"{htmlbody}</ul></details>"
        htmltext = '<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>'
        htmltext = f'{htmltext}{kml_file}</title><body>'
        htmltext = f'{htmltext}{htmlbody}</body><html>'
       
        if not os.path.exists(expDir):
            os.mkdir(expDir)
        with open(exp_file, mode = "w", encoding='utf-8') as file: 
            file.write(htmltext)
