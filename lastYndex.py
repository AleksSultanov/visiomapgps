import requests
import os
from dotenv import load_dotenv

""" VisioMapYandex
    version="1.0.0"
   Получение последних загрузок на ЯндексДиск по токену
   Файлы с результатами
   yandexLastUpload.txt - последние загруженные
   yandexLastPublic.txt - последние опубликованные

"""

RESULT_LIMIT   = 100
UPLOAD_URL     = "https://cloud-api.yandex.net/v1/disk/resources/upload" 
LASTUPLOAD_URL = "https://cloud-api.yandex.net/v1/disk/resources/last-uploaded"
PUBLISH_URL    = "https://cloud-api.yandex.net/v1/disk/resources/publish"
PUBLIC_URL     = "https://cloud-api.yandex.net/v1/disk/resources/public"

def urlHeader():
    key = os.getenv("YANDEXKEY", "0")
    if key == "0":
        print("not_found_key")
    return {"Accept": "application/json","Authorization": "OAuth " + key}


#Загрузка переменных
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
if os.path.exists(dotenv_path):
     load_dotenv(dotenv_path)

r = requests.get(url=LASTUPLOAD_URL, params=None, headers=urlHeader())
with open('yandexLastUpload.txt', 'wb') as f:
    f.write(r.content)


r = requests.get(url=PUBLIC_URL, params=None, headers=urlHeader())
with open('yandexLastPublic.txt', 'wb') as f:
    f.write(r.content)
    