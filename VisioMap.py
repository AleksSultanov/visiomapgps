""" VisioMap
    version="1.0.0"

    Модуль для создание карты визуализации треков
    Для работы необходимо в текущей папке наличие папок fotoYYY и gpxYYY
      , где YYY - произвольное имя - имя карты.
    Каталог fotoYYY (необязателен) - содержит фотографии для карты, которые будут загружены на ЯндекскДиск
     и отображены на карте
    Каталог gpxYYY - содержит треки в формате GPX для карты

    Для каждого YYY создается своя карта в каталоге map, имя карты mapYYY.html 
"""
import os
import sys
import re
import time
import folium
import csv
from folium.plugins import MousePosition
from folium.plugins import MiniMap
from folium import plugins
from datetime import datetime
from dotenv import load_dotenv
import copy

from mapLib.mapcfg import reedAttr
from mapLib.mapparse import attrFile, parseGPX, struct_imageFull, parseImage, parseKmlFolder
import mapLib.mapAdd as _madd
import mapLib.reqYandex as yandx
from mapLib.setform import SetForm

_PRINT_STATUS_ERR = True

#Функции для загрузки в интернет
def uploaDisk(file):
    status_code, err_text = yandx.uploaDisk(file)
    if status_code not in [200,201] and _PRINT_STATUS_ERR:
       print(f"[{status_code}] ошибка загрузки файла {file}")     
       print(err_text)     
    return status_code

def readDisk(filesData):
    res_filesLink = {}
    status_code, err_text = yandx.publicDisk()
    if status_code == 200:
        status_code, resData ,err_text = yandx.readDisk()
        if status_code == 200:
            for data in resData:
                if "public_url" in data.keys() and "name" in data.keys():
                    _link  = data["public_url"]                    
                    _prev  = f"https://getfile.dokpub.com/yandex/get/{_link}" 
                    row = {"preview":_prev,"link":_link}    
                    res_filesLink.setdefault(data["name"],row)
            if len(res_filesLink) == 0:
                return filesData    

            sDate = str(datetime.now()) 
            for _data in filesData:
                if _data["file"] in res_filesLink.keys():
                    
                    _data["link"] = res_filesLink[_data["file"]]["link"]
                    _data["preview"] = res_filesLink[_data["file"]]["preview"]
                    _data["udateinternet"] = sDate

    if status_code != 200 and _PRINT_STATUS_ERR:
       print(f"[{status_code}] ошибка публикации или чтения файлов")     
       print(err_text)     
    return filesData        
        


# Функция для разбора фото в каталоге

def getCashe(file,existsfilesData):
    result = struct_imageFull().copy()
    result["file"] = file
    for old_row in existsfilesData:
        if old_row["file"] == file:
            return old_row
    return result        

def parseImageDir(map_Dir, issave:bool=True):
    result = {"count":0,"uploadcount":0}
    upCnt = 0
    fileCsv =os.path.join(map_Dir,'result.csv')
    if not 'win_excel' in csv.list_dialects():
        csv.register_dialect('win_excel', delimiter=';', quoting=csv.QUOTE_NONE)
    start_time = time.time() 
    if not os.path.isdir(map_Dir):
        print(f'Нет каталога по указанному пути "{map_Dir}"')
        return result
    lstdir  = os.listdir(map_Dir) 
    filesData = []
    existsfilesData = []
    if os.path.exists(fileCsv):
        with open(fileCsv,'r', newline='') as csvfile:
            reader = csv.DictReader(csvfile,dialect='win_excel')
            for data in reader:
                existsfilesData.append(data)
    
    for l in lstdir:
        file = os.path.join(map_Dir,l)
        if file == fileCsv:
            continue
        #сначала смотрим старый результат
        _row = getCashe(file,existsfilesData)
        if _row ["udategps"] != None :
            filesData.append(_row) 
        else:
            _gprrow = parseImage(file)
            for key in _gprrow:
               _row[key] = _gprrow[key] 
            filesData.append(_row)
    gpx_time = time.time()       
    dur_gpx_time = round(gpx_time -  start_time,2) 
    #Отправка на сервер
    ifUpload = False
    for urldate in filesData:
        file  = urldate["file"]
        _row = getCashe(file,existsfilesData)
        if _row["link"] == None or _row["link"].strip() == "":
           sDate = str(datetime.now())   
           print(f"{sDate} Загрузка на сервер файла {file} ")
           uploaDisk(file)  
           upCnt += 1              
           ifUpload = True
    # ifUpload = True       
    if ifUpload:   
        sDate = str(datetime.now())   
        print(f"{sDate} Публикация и обновление ссылок на фотографии . .")
        filesData = readDisk(filesData)  
    cnt =len(filesData) 
    if issave:
        fieldnames = filesData[0].keys()
        with open(fileCsv, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames,
                                    extrasaction='ignore',
                                    dialect='win_excel')
            writer.writeheader()
            for _row in filesData:
                writer.writerow(_row)
   
    dur_upp_time = round(time.time() -  gpx_time,2)
    dur_all_time = round(time.time() -  start_time,2)
    result = {"count":cnt,"uploadcount":upCnt,
              "filesData":filesData,
              "dur_gpx_time":dur_gpx_time,
              "dur_upp_time":dur_upp_time,
              "dur_all_time":dur_all_time
             } 
    return result

#Для тестов
def tofile(fname,list):
    print(fname)    
    if not 'win_excel' in csv.list_dialects():
        csv.register_dialect('win_excel', delimiter=';', quoting=csv.QUOTE_NONE)
    fieldnames = list[1].keys()
    with open(fname+'.csv', 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames,
                 extrasaction='ignore',
                 dialect='win_excel')
        writer.writeheader()
        for _row in list:
                writer.writerow(list[_row])


#Запуск
startDir  = os.listdir()
MapDirSave = 'map'
CountDir = {"gpx":0,"foto":0,"map":0,"gpxM":0,"fotoM":0}
MapDirectory = {}
_ZOOM_START = 10
_FOTO_ON    = False
_AUTO_ON    = False 
_WHEATHER   = True
_MOUSEPOSON = True
_MINIMAP    = True
_MAPlAYER   = {}   
_TZHOUR     = 0
_TZMINUTE   = 0
_GPXCSV     = False  #Формировать csv файл с расшифровкой вычислений gpx

#Загрузка переменных
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
if os.path.exists(dotenv_path):
     load_dotenv(dotenv_path)

if '-gui' in sys.argv:    
    sform = SetForm('SettingForm')
    sform.show()
    if sform.modalResult != 'Ok':
        exit()
    fsettings = sform.values   
    _ZOOM_START = int(fsettings['zoom'])
    _FOTO_ON    = bool(fsettings['fotoOn'])
    _AUTO_ON    = bool(fsettings['autoOn'])
    _WHEATHER   = bool(fsettings['wheatherShow'])
    _MOUSEPOSON = bool(fsettings['mouseposOn'])
    _MINIMAP    = bool(fsettings['minimapOn'])
    _MAPlAYER   = fsettings['MapLayerbox']
    _TZHOUR     = int(fsettings['tzh'])
    _TZMINUTE   = int(fsettings['tzmin'])
else:        
    if '-fotoOn' in sys.argv:
        _FOTO_ON = True
    if '-AutoOn' in sys.argv:
        _AUTO_ON = True
    if '-ZoomOn' in sys.argv:
        _ZOOM_START = 14


for d in startDir:
    if os.path.isdir(d) and (d.upper().find("GPX") == 0 or d.upper().find("FOTO") == 0) :
        isEmpty = False
        mapName = "map"+d[3:]
        if d.upper().find("FOTO") == 0:
            mapName = "map"+d[4:]
        if mapName not in  MapDirectory.keys():
            MapDirectory.setdefault(mapName,{d:{"files":{}}})
        else:
            MapDirectory[mapName].setdefault(d,{"files":{}})


root_time = time.time() 

a = reedAttr()
if _TZHOUR !=0 : 
    a["timezone"]["hours"] = _TZHOUR
if _TZMINUTE !=0 :     
    a["timezone"]["minutes"] = _TZMINUTE
for _mapName in MapDirectory:
    _map = folium.Map(location=None,tiles=None,zoom_start=10)
    for _dirName in MapDirectory[_mapName]:
        lstdir  = os.listdir(_dirName) 
        for l in lstdir: 
            _row = attrFile(a,l)
            if _dirName.upper().find("FOTO") == 0:
                _row["type"] = "foto"
            if _row["type"] in MapDirectory[_mapName][_dirName]["files"].keys():    
                MapDirectory[_mapName][_dirName]["files"][_row["type"]].append(_row)
            else:
                MapDirectory[_mapName][_dirName]["files"].setdefault(_row["type"],[_row])     

for _mapName in MapDirectory:
    weather_locations = []
    foto_locations = []
    CountDir["map"] += 1
    CountDir["gpxM"]  = 0
    CountDir["fotoM"] = 0

    mapObjectList = []
    for _dirName in MapDirectory[_mapName]:
        typelist = MapDirectory[_mapName][_dirName]["files"].keys()
        for _type in typelist:
            if _type == "foto":
                sDate = str(datetime.now())   
                print(f"{sDate} обработка фото в папке {_dirName} ")
                res_parse = parseImageDir(_dirName)
                foto_locations = res_parse["filesData"]
                sDate = str(datetime.now()) 
                f_cnt =  res_parse["count"]
                f_upcnt =  res_parse["uploadcount"]
                f_time = res_parse["dur_gpx_time"]
                f_itipme = res_parse["dur_upp_time"]
                CountDir["fotoM"] =  f_cnt
                print(f"{sDate} Обработано {f_cnt} фото. Загруженно на сервер {f_upcnt}. Время обработки :{f_time}, время загрузки: {f_itipme}")
                for row in foto_locations:
                    _lat = row["latitude"]
                    _lon = row["longitude"]
                    startlocation =[ _lat,_lon]
                    break
                continue
            cnt_type  = len(MapDirectory[_mapName][_dirName]["files"][_type])
            groupfl = (cnt_type > 2)
            #Если файлов по категории больше 2, создадим группу для FeatureGroup
            if groupfl:
                isShow = not (_type == 'auto' and not _AUTO_ON and 'empty' in typelist)
                captionGr = _madd.getCaptionGr(a,_type)
                fgg = folium.FeatureGroup(name=captionGr, show=isShow)
                mapObjectList.append(fgg)
            fglist = {}
            isKML = False    
            for _file in MapDirectory[_mapName][_dirName]["files"][_type]:
                file = os.path.join(_dirName, _file["file"])
                dataDict = {}
                caption = _madd.getCaption(a,_file)
                if file[-3::].upper() == 'GPX':
                    dataDict = parseGPX(file)
                if file[-3::].upper() == 'KML':
                    dataDict = parseKmlFolder(file)  
                    isKML = True
                if len(dataDict) != 0:
                    for _key in dataDict: 
                        result, locations = dataDict[_key] 
                        sDate = str(datetime.now()) 
                        if isKML:  
                            sPrintFile = f"{file} - {_key}"
                        else:    
                            sPrintFile = file
                        print(f"{sDate} обработка трека {sPrintFile} ")
                        if _GPXCSV:
                            tofile(_file["file"][:-4].strip(),locations)
                        if result == 0:
                            print(f"Пустой трек {sPrintFile}")
                            continue
                        CountDir["gpx"]  += 1  
                        CountDir["gpxM"] += 1     
                        #Создание FeatureGroup\FeatureGroupSubGroup для каждого файла
                        if isKML: 
                            caption = _key       
                        isExistFg = False
                        for key in fglist:
                            if key == caption:
                                isExistFg = True
                                fg = fglist[key]
                        if not isExistFg:
                            if groupfl:
                                fg = plugins.FeatureGroupSubGroup(fgg,caption)
                            else:    
                                fg = folium.FeatureGroup(name=caption, show=True)
                            fglist.setdefault(caption,fg)
                            mapObjectList.append(fg)  
                        startlocation =[ locations[1]["lat"],locations[1]["lon"] ]
                        maxPoint = len(locations)
                        Finishlocation = [ locations[maxPoint]["lat"],
                                            locations[maxPoint]["lon"] ]  
                        if _WHEATHER:                                     
                            weather_locations.append(startlocation)
                            weather_locations.append(Finishlocation)
                        locatDict = copy.deepcopy(_file)
                        
                        locatDict.setdefault("locations",locations)
                        #Рисуем трек
                        _madd.trackPoint(a,_map,locatDict,fg) 
                        #Рисуем интервальные точки 
                        _madd.intervalPoint(a,_map,locatDict,fg)

    if CountDir["gpxM"] == 0 and CountDir["fotoM"] == 0 :
        print(f"Нет данных для создания карты {_mapName}")
    else:  
        #создание карты
        _map = folium.Map(location=startlocation,
            tiles=None,
            zoom_start=_ZOOM_START)  
        #Слои карты         
        folium.TileLayer('OpenStreetMap', name="OpenStreet Map").add_to(_map)
        for _m in _MAPlAYER:
            _dict = _MAPlAYER[_m]
            folium.TileLayer(_dict['tiles'], name=_m, attr=_dict['attr'], 
            min_zoom=_dict['min_zoom'],max_zoom=_dict['max_zoom'],
            subdomains=_dict['subdomains']
            ).add_to(_map)

        if len(weather_locations) >0 :
            captionW = _madd.getCaptionGr(a,"weather")   
            wfg = folium.FeatureGroup(name=captionW, show=False).add_to(_map)        
            _madd.addWeather(a,_map,weather_locations,wfg)
        if len(foto_locations) >0 :
            captionF = _madd.getCaptionGr(a,"foto")  
            fotofg = folium.FeatureGroup(name=captionF, show=(CountDir['gpxM']==0 or _FOTO_ON)).add_to(_map)        
            _madd.addFotos(a,_map,foto_locations,fotofg)

        for _obj in mapObjectList:
            _map.add_child(_obj)
        
        mapFile = _mapName + '.html'
        # Добавление панелей на карту"            
        plugins.Fullscreen(position="topright",
            title="Просмотр на полном экране",
            title_cancel="Нажми для выхода",
            force_separate_button=True,
            ).add_to(_map)
        folium.LayerControl(collapsed=True).add_to(_map)  
        if _MOUSEPOSON:
            MousePosition().add_to(_map)
        if _MINIMAP:
            _minimap = MiniMap(tile_layer="Stamen Terrain") 
            _map.add_child(_minimap)   
        #сохраняем карту 
        if not os.path.exists(MapDirSave):
            os.mkdir(MapDirSave)
        mapFile = os.path.join(MapDirSave,mapFile)      
        _map.save(mapFile)
        sDate = str(datetime.now())   
        print(f"{sDate} Создана карта {mapFile} .Кол-во треков: {CountDir['gpxM']} фотографий: {CountDir['fotoM']}")
        
finish_time = round(time.time() - root_time,2)
print(f"Обработано треков: {CountDir['gpx']}, создано карт: {CountDir['map']}, время на все операции {finish_time} сек")