"""
    version="1.0.0"
    Модуль для функций парсинга файлов с треками

"""

import gpxpy
from geopy.distance import lonlat, distance
from exif import Image
import re
from datetime import datetime
import xml.etree.ElementTree as ET 

calcrowGpx = {'time': 0,
              'lat': 0,
              'lon': 0, 
              'ele': 0, 
              'star_time': 0,
              'star_ele': 0,
              'pre_lat': 0,
              'pre_lon': 0,
              'pre_dist': 0,
              'pre_dist2': 0,
              'max_ele':  0,
              'point1': None,
              'point2': None,
              't_distele': 0,
              'distance': 0,
              'distance2': 0,
              'duration': 0,
              'climb': 0,
              'climbMax': 0}

def struct_imageGps():
    result = {"emptyGPX":1,
            "file":None,
            "altitude":None,
            "datestamp":None,
            "latitudeDM":None,
            "longitudeDM":None,
            "timestamp":None,
            "latitude":None,
            "longitude":None,
            "udategps":None
            }
    return result

def struct_imageInt():
    result = {"link":None,
            "preview":None,
            "udateinternet":None}
    return result    

def struct_imageFull():
    result = struct_imageGps().copy()
    for key in struct_imageInt():
        result.setdefault(key,None)
    result.setdefault("description",None)
    return result    


def dm_to_deg(args):
    """Преобразует градусы в формате DM в формат DEG"""
    degrees, minutes, seconds = args
    if degrees >= 0:
        decimal = degrees + minutes/60.0 + seconds/3600.0
    else:
        decimal = degrees - minutes/60.0 - seconds/3600.0
    return decimal

def checktype(a,t_type):
    if t_type in a.keys():
        return t_type
    else:
        return "empty" 

def refind(rexp,txt):
    m = re.findall(rexp,txt)
    if m == None or len(m) == 0:
        return "empty"
    else:
        return m[0].strip()

def attrFile(a,file):
        rexpFile = re.compile(r"([^\\]+)\.[a-z]{3}$")
        rexpType = re.compile(r"\(([^\(\)]*)\)\.[a-z]{3}$")
        rexpDate = re.compile(r"\[([0-9]{4}-[0-9]{2}-[0-9]{2})\]")
        rexpRep1 = re.compile(r"\(.*\)$")
        rexpRep2 = re.compile(r"\[[^\[]*\]")
        tr_name = refind(rexpFile,file)
        tr_type = checktype(a,refind(rexpType,file))
        tr_date = refind(rexpDate,file)
        tr_name = rexpRep1.sub("",tr_name)
        tr_name = rexpRep2.sub("",tr_name).strip("_(-")
        return {"file":file,
                "type":tr_type,
                "date":tr_date,
                "name":tr_name}


def _nvl(gpxprm):
    if gpxprm == None:
        return 0
    else:
        return gpxprm  


def startGPSattr(calcrow):
    """Установка начальных значений для показателей по координатам GPS"""    
    calcrow['star_time'] = calcrow['time']
    calcrow['star_ele']  = calcrow['ele']
    calcrow['pre_time']  = calcrow['time']
    calcrow['pre_ele']   = calcrow['ele']
    calcrow['max_ele']   = calcrow['ele']
    calcrow['pre_lat']   = calcrow['lat']
    calcrow['pre_lon']   = calcrow['lon']

def preGPSattr(calcrow):
    """Установка предыдущих значений для показателей по координатам GPS"""    
    calcrow['pre_lat'] = calcrow['lat']
    calcrow['pre_lon'] = calcrow['lon']  
    calcrow['pre_time'] = calcrow['time']
    calcrow['pre_ele'] = calcrow['ele']
    calcrow['pre_dist'] = calcrow['distance']
    calcrow['pre_dist2'] = calcrow['distance2']                 

def calcGPSattr(calcrow):
    """Вычисления показателей по координатам GPS"""    
    #высоты
    if calcrow['max_ele'] < calcrow['ele']:
        calcrow['max_ele'] = calcrow['ele']
    calcrow['t_distele'] = round(calcrow['pre_ele']-calcrow['ele'],2)
    calcrow['climb'] = round(calcrow['ele']-calcrow['star_ele'],2)
    calcrow['climbMax'] = round(calcrow['max_ele']-calcrow['ele'],2)
    #расстояние между точками
    if calcrow['lat'] < 90 and calcrow['lon'] < 90:
        #distance c координатами после 90 не работает
        calcrow['point1']  = lonlat(calcrow['pre_lat'], calcrow['pre_lon'])
        calcrow['point2']  = lonlat(calcrow['lat'], calcrow['lon'])
        calcrow['t_dist_tmp']= distance(calcrow['point1'], calcrow['point2']).m 
        calcrow['distance'] = round(calcrow['t_dist_tmp'] + calcrow['pre_dist'],2)
        if calcrow['t_distele'] == 0:
            calcrow['distance2'] = round(calcrow['t_dist_tmp'] + calcrow['pre_dist2'],2) 
        else:
        #растояние с учетом высоты по теореме Пифагора.    
            calcrow['distance2'] = round((calcrow['t_dist_tmp']**2+abs(calcrow['t_distele'])**2)**0.5+ calcrow['pre_dist2'],2)   
    #время
    calcrow['duration'] = (calcrow['time'] - calcrow['star_time'])



def parseGPX(file):
    n = 0
    calcrow = calcrowGpx.copy() 
    """Построение словаря с треками из файла gpx """
    with open(file, 'r',encoding='UTF-8') as gpx_file:
        gpx = gpxpy.parse(gpx_file)
    result = {}
    resultTr = {}
    for track in gpx.tracks:
        for segment in track.segments:
            for point in segment.points:
                n += 1
                # Координаты, время и высота
                calcrow['time'] = _nvl(point.time)
                calcrow['lat']  = _nvl(point.latitude)
                calcrow['lon']  = _nvl(point.longitude)
                calcrow['ele']  = _nvl(point.elevation)

                # Фиксируем начальные значения
                if n == 1:
                    startGPSattr(calcrow)
                #вычисления
                calcGPSattr(calcrow)
                resultrow = calcrow.copy() 
                #Обновляю предыдущие значения 
                preGPSattr(calcrow)
                #результат 
                resultTr[n]=resultrow
    result[file] = n, resultTr              
    return result 

####### IMAGE ###########
def parseImage(file):
    """Получение данных gps из изображения"""
    sDate = str(datetime.now()) 
    result = struct_imageGps().copy()
    result["file"] = file
    result["udategps"] = sDate
    with open(file, "rb") as _file:
        foto_image = Image(_file)
    if foto_image.has_exif:
        if foto_image.get("gps_latitude") != None and foto_image.get("gps_longitude") != None:
            result["emptyGPX"] = 0
            result["altitude"] = foto_image.get("gps_altitude")
            result["datestamp"] = foto_image.get("gps_datestamp")
            result["latitudeDM"] = foto_image.get("gps_latitude")
            result["longitudeDM"] = foto_image.get("gps_longitude")
            result["timestamp"] = foto_image.get("gps_timestamp")
            result["latitude"] = dm_to_deg(foto_image.get("gps_latitude"))
            result["longitude"] =dm_to_deg(foto_image.get("gps_longitude"))
    return result 

####### KML ######

'''Получение тега узла без Namespace (http://www.opengis.net/kml/2.2)
   Параметры:
     subelem - узел
'''
def kml_gettag(subelem):
    return re.sub('\{.*\}', '', subelem.tag)

'''Получение текста элемента по пути
   Параметры:
     elem - начальный узел
     list - путь к значению(список)
'''

def kml_pathparse(elem, list):
    if len(list) == 0:
        return None
    for subelem in elem:
        subTag =kml_gettag(subelem)
        if subTag == list[0]:
            if len(list[1::]) > 0 :
                return kml_pathparse(subelem,list[1::])
            else:  
                return subelem.text
    return None  

'''Получение словаря с данными узел-текст узла по пути
   Парметры:
     elem - начальный узел
     list - путь к узлу (список)
'''
def kml_pathparseDict(elem, list):
    l_keys = []
    result = {}
    n = 0
    if len(list) == 0:
        return 0,None
    for subelem in elem:
        subTag =kml_gettag(subelem)
        if subTag == list[0]:
            if len(list[1::]) > 0 :
                return kml_(subelem,list[1::])
            else: 
                for subelem2 in subelem:
                    subTag2 = kml_gettag(subelem2) 
                    l_keys.append(subTag2)
                for _keys in set(l_keys):
                    n = 0
                    dictval = {}
                    for subelem3 in subelem:
                        subTag3 = kml_gettag(subelem3) 
                        if _keys == subTag3: 
                            n += 1
                            dictval[n] = subelem3.text
                            result[_keys] = dictval
                return n,result
    return 0,None 

'''Обработка узла Placemark в KML файле
   Парметры:
     elem - начальный узел
     fdict - словарь с результатом
'''
def kml_placemarkparse(subelem, fdict, track_name):
    calcrow = calcrowGpx.copy() 
    if track_name == None:
        _name = kml_pathparse(subelem, ['name'])
        fdict[_name] = {}
    else:
        _name = track_name
    placename = kml_pathparse(subelem,['name'])
    coordinates = kml_pathparse(subelem,['Point','coordinates'])
    if coordinates == None:
        longitude = None
        latitude = None
        altitude = 0
    else:    
        longitude = coordinates.split(',')[0]
        latitude = coordinates.split(',')[1]
        altitude = coordinates.split(',')[2]
    tr_cnt = 0    
    tr_cnt, tracks = kml_pathparseDict(subelem,['Track']) 
    tracksDict = {}
    n = 0
    if tr_cnt != 0 : 
        for key in tracks['coord']:
            coordinates = tracks['coord'][key]
            c_time = tracks['when'][key]
            c_lon = coordinates.split(' ')[0]
            c_lat = coordinates.split(' ')[1]
            c_ele = coordinates.split(' ')[2]
            n += 1
            # Координаты, время и высота
            calcrow['time'] = datetime.strptime(c_time, '%Y-%m-%dT%H:%M:%Sz')
            calcrow['lat']  = float(c_lat)
            calcrow['lon']  = float(c_lon)
            calcrow['ele']  = float(c_ele)

            # Фиксируем начальные значения
            if n == 1:
                startGPSattr(calcrow)
            #вычисления
            calcGPSattr(calcrow)
            resultrow = calcrow.copy() 
            #Обновляю предыдущие значения 
            preGPSattr(calcrow)
            #результат 
            tracksDict[key] = resultrow  

    styleUrl = kml_pathparse(subelem,['styleUrl'])
    fdict[_name][placename] = {"longitude":longitude,
                                "latitude":latitude,
                                "altitude":altitude,
                                "styleUrl":styleUrl,
                                "tracks":tracksDict,
                              }


'''Обработка узла Folder в KML файле
   Парметры:
     elem - начальный узел
     fdict - словарь с результатом
'''
def kml_folderparse(elem, fdict):
    _name = kml_pathparse(elem, ['name'])
    fdict[_name] = {}
    for subelem in elem:
        subTag = kml_gettag(subelem)
        if subTag == 'Folder':
            kml_folderparse(subelem, fdict)
        if subTag == 'Placemark':
            kml_placemarkparse(subelem, fdict,_name)

'''Парсинг KML файла
   Параметры:
     kml_file - kml файл
   Результат:
     Список словари: Style, StyleMap, Folder
'''
def parseKml(kml_file):
    styledict = {}
    styleMapdict = {}
    folderDict = {}
    if kml_file[-3::].upper() == 'KML': 
        root_node = ET.parse(kml_file).getroot()
        for elem in root_node:
            for subelem in elem:
                subTag =kml_gettag(subelem)
                if subTag == 'Style':
                    _value = kml_pathparse(subelem,['IconStyle','Icon','href'])
                    styledict[subelem.attrib['id']] = _value
                if subTag == 'StyleMap':
                    _value = kml_pathparse(subelem,['Pair','styleUrl'])
                    styleMapdict[subelem.attrib['id']] = _value
                if subTag == 'Folder':
                    kml_folderparse(subelem, folderDict) 
                if subTag == 'Placemark':
                    kml_placemarkparse(subelem, folderDict, None) 

    return styledict,styleMapdict, folderDict   

def parseKmlFolder(kml_file):
    result = {}
    styledict, styleMapdict,folderDict = parseKml(kml_file)
    for _n in folderDict:
        for _p in folderDict[_n]:
            _val = folderDict[_n][_p]["tracks"]
            result[_p] = len(_val),_val
    return result        

