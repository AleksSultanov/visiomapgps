""" 
    version="1.0.0" 
    Модуль для функций добавления атрибутов на карту

"""

from datetime import timedelta
import folium

def getTtype(a,dict):
    t_type = dict["type"]    
    if t_type in a.keys():
        return t_type
    else:
        return "empty"      

def getCaption(a,dict, isfull:bool = True ):
    caption = dict["name"]
    if isfull:
        t_date = dict["date"]
        if t_date != "empty":
            caption = f"{caption}  {t_date}"
        t_type = getTtype(a,dict) 
        txt_type = a[t_type]["caption"]       
        caption = f"{caption} {txt_type}" 
    return caption  

def getCaptionGr(a,_type):
    return a[_type]["caption_gr"]       

def mapsUrl(g_lat, g_lon):
    C_TARGET = 'target = "_blank"'
    C_ICON_W = 'width="20"'
    C_SCALE = 16
    urlGoogle  = f'<a href = "http://www.google.com/maps?q={g_lat},{g_lon}" {C_TARGET}><img src="http://www.google.com/favicon.ico" {C_ICON_W} ></a>'
    urlOstreet = f'<a href = "https://www.openstreetmap.org/#map={C_SCALE}/{g_lat}/{g_lon}" {C_TARGET}><img src="https://openstreetmap.org/favicon.ico" {C_ICON_W} ></a>'
    urlOSMAND  = f'<a href = "https://osmand.net/go?lat={g_lat}&lon={g_lon}&z={C_SCALE}" {C_TARGET}><img src="https://osmand.net/images/favicons/favicon.ico" {C_ICON_W} ></a>'
    urlYandex  = f'<a href = "https://yandex.ru/maps?pt={g_lon},{g_lat}&z={C_SCALE}" {C_TARGET}><img src="https://yandex.ru/maps/favicon.ico" {C_ICON_W} ></a>'
    urlWindy   = f'<a href = "https://windy.com/{g_lat}/{g_lon}&z={C_SCALE}" {C_TARGET}><img src="https://windy.com/favicon.ico" {C_ICON_W} ></a>'
   
    url =f'{urlGoogle}{urlOstreet}{urlOSMAND}{urlYandex}{urlWindy}'
    return url

def getPopupPoint(a,dict,point, isfull:bool = False):
    def toDistStr(value):
        if value > 0:
            if value > 1000:
                return str(round(value/1000,2))+' км'
            else:    
                return str(value)+' м'
        else:
            return "0"

    caption = getCaption(a,dict, isfull = False)
    locations = dict["locations"]
    t_ele = locations[point]["ele"]
    t_lat = locations[point]["lat"]
    t_lon = locations[point]["lon"]
    t_climb = locations[point]["climb"]
    t_date = locations[point]["time"]
    t_datel = t_date + timedelta(hours=a["timezone"]["hours"],minutes=a["timezone"]["minutes"])
    txt_date = t_datel.strftime("%d.%m.%Y %H:%M")
    t_dur = locations[point]["duration"]
    t_dist = toDistStr(locations[point]["distance"])
    t_dist2 = toDistStr(locations[point]["distance2"])
    urls =  mapsUrl(t_lat,t_lon) 

    result = f'<nobr><font color="purple"><b>{caption}</b></font></nobr>'
    result = f'{result}<br><br><table>'
    result = f'{result}<tr><td><b>Высота:</b></td><td>{round(t_ele,2)} м</td></tr>'
    if str(t_climb) != '0':
        result = f'{result}<tr><td>изменение:</td><td>{round(t_climb,2)} м</td></tr>'
    result = f'{result}<tr><td><b>Время:</b></td><td><nobr>{txt_date}</nobr></td></tr>'
    if str(t_dur) !="0":
        result = f'{result}<tr><td>в пути:</td><td>{t_dur}</td></tr>'
    if t_dist !="0":
        result = f'{result}<tr><td><b>Расстояние:</b></td><td></td></tr>'
        result = f'{result}<tr><td>рассчет 1:</td><td>{t_dist}</td></tr>'
        if t_dist2 !="0":
            result = f'{result}<tr><td>рассчет 2:</td><td>{t_dist2}</td></tr>'
    result = f'{result}<tr><td><b>Координаты:</b></td><td></td></tr>'
    result = f'{result}<tr><td>широта:</td><td>{t_lat}</td></tr>'
    result = f'{result}<tr><td>долгота:</td><td>{t_lon}</td></tr>'
    result = f'{result}<tr><td><br></td><td></td></tr>'
    result = f'{result}<tr><td><b>На картах:</b></td><td><nobr>{urls}</nobr></td></tr>'
    result = f'{result}</table>'
    return result  

def addWeather(a,_map,weather_locations,wfg):
    locations_tmp = set(map(lambda x: f"{round(x[0],2)};{round(x[1],2)}" ,weather_locations ))
    locations = list(map(lambda x:x.split(";"),locations_tmp))
    for _lat,_lon in locations:
        t_frame = """
            <iframe width="400" height="400"
            src="https://embed.windy.com/embed2.html?"""
        t_frame = f"{t_frame}lat={_lat}&lon={_lon}&detailLat={_lat}&detailLon={_lon}"    
        t_frame += """1&width=400&height=400&zoom=11&level=surface&overlay=wind&product=ecmwf&menu=&message=&marker=true&calendar=now&pressure=&type=map&location=coordinates&detail=true&metricWind=km%2Fh&metricTemp=%C2%B0C&radarRange=-1"
            frameborder="0"></iframe>  """

        popuptxt = t_frame
        t_type = "weather"
        folium.Marker(location=[_lat,_lon], 
            popup = popuptxt, 
			icon=folium.Icon(color=a[t_type]["icon_fill_color"],
						 icon_color=a[t_type]["icon_color"],
						 icon=a[t_type]["icon_icon"],
						 prefix=a[t_type]["icon_prefix"],
						 icon_size = a[t_type]["icon_size"],
						 shadow_size = a[t_type]["icon_shadow_size"],
						 icon_anchor = [0,0]
						 )
							
            ).add_to(_map).add_to(wfg)  
            

def trackPoint(a,_map,dict,fg):
    t_type = getTtype(a,dict)
    locations = dict["locations"]
    #Рисую трек 
    points = []
    for key in locations:         
      points.append((locations[key]["lat"],locations[key]["lon"]))  
    folium.PolyLine(points,
        color=a[t_type]["line_color"],
        weight=a[t_type]["line_weight"], 
        opacity=a[t_type]["line_opacity"]
        ).add_to(_map).add_to(fg)         

def intervalPoint(a,_map,dict,fg,interval_point:int=10):
    locations = dict["locations"]
    t_type = getTtype(a,dict)
    if t_type == "empty":
        t_type = "man"  
    #Рисую кружки
    max = len(locations)
    step = round(max/interval_point)
    isIcon = True
    for i in range(1,max,step):
        rowlocation = [locations[i]["lat"],locations[i]["lon"]]
        popuptxt = getPopupPoint(a,dict,i, isfull = True)
        folium.CircleMarker(
            location=rowlocation, 
            radius = a[t_type]["circle_radius"], 
            popup = popuptxt, 
            fill_color=a[t_type]["circle_fill_color"], 
            color=a[t_type]["circle_color"], 
            fill_opacity = a[t_type]["circle_fill_opacity"]
            ).add_to(_map).add_to(fg)
            
        #Рисую значки    
        if isIcon:
            popuptxt = getPopupPoint(a,dict,i, isfull = True)
            folium.Marker(location=rowlocation, 
                popup = popuptxt, 
                icon=folium.Icon(color=a[t_type]["line_color"],
                             icon_color=a[t_type]["icon_color"],
                             icon=a[t_type]["icon_icon"],
                             prefix=a[t_type]["icon_prefix"],
                             icon_size = a[t_type]["icon_size"],
                             shadow_size = a[t_type]["icon_shadow_size"],
                             icon_anchor = [0,0]
                             )
                ).add_to(_map).add_to(fg)  
        isIcon = not isIcon 
    if isIcon: # Если последняя точка не попала под значек
        rowlocation = [locations[max]["lat"],locations[max]["lon"]]
        popuptxt = getPopupPoint(a,dict,max, isfull = True)
        folium.Marker(location=rowlocation, 
                popup = popuptxt, 
                icon=folium.Icon(color=a[t_type]["line_color"],
                             icon_color=a[t_type]["icon_color"],
                             icon=a[t_type]["icon_icon"],
                             prefix=a[t_type]["icon_prefix"],
                             icon_size = a[t_type]["icon_size"],
                             shadow_size = a[t_type]["icon_shadow_size"],
                             icon_anchor = [0,0]
                             )
                ).add_to(_map).add_to(fg)  


def addFotos(a,_map,foto_locations,fotofg):
    for row in foto_locations:
        _lat = row["latitude"]
        _lon = row["longitude"]
        _prev = row["preview"]
        _link = row["link"]
        t_type = "foto"
        if _lat == None or _lon == None or _link == None:
            continue
        if str(_lat).strip() == "" or str(_lon).strip() == "" or _link.strip() == "":
            continue
        popuptxt = f'<div> <img height="200"src="{_prev}"></div>'
        popuptxt = f'{popuptxt}<hr><a href = "{_link}" target = "_blank">Полный размер </a>'
        urls =  mapsUrl(_lat,_lon) 
        popuptxt = f'{popuptxt}<br>Точка на картах: {urls}'
        folium.Marker(location=[_lat,_lon], 
            popup = popuptxt, 
			icon=folium.Icon(color=a[t_type]["icon_fill_color"],
						 icon_color=a[t_type]["icon_color"],
						 icon=a[t_type]["icon_icon"],
						 prefix=a[t_type]["icon_prefix"],
						 icon_size = a[t_type]["icon_size"],
						 shadow_size = a[t_type]["icon_shadow_size"],
						 icon_anchor = [0,0]
						 )
							
            ).add_to(_map).add_to(fotofg)
