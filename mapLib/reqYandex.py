""" reqYandex
    version="1.0.0"
    Модуль для методов работы с Яндекс Диском

"""
import requests
import os

RESULT_LIMIT   = 100
UPLOAD_URL     = "https://cloud-api.yandex.net/v1/disk/resources/upload" 
LASTUPLOAD_URL = "https://cloud-api.yandex.net/v1/disk/resources/last-uploaded"
PUBLISH_URL    = "https://cloud-api.yandex.net/v1/disk/resources/publish"
PUBLIC_URL     = "https://cloud-api.yandex.net/v1/disk/resources/public"


def urlHeader():
    key = os.getenv("YANDEXKEY", "0")
    if key == "0":
        print("ERROR_not_found_yandex_key")
    return {"Accept": "application/json","Authorization": "OAuth " + key}

def publicPrm():
    return {"limit":RESULT_LIMIT
            ,"fields":"items.name,items.public_url,items.path"}

def uploaDisk(file):
    params = {'path':file,'overwrite':True}
    r = requests.get(url=UPLOAD_URL, params=params, headers=urlHeader())
    if r.status_code == 200:
        res = r.json()
        # Отправляем локальный файл
        upload = requests.put(url=res['href'], data=open(file, 'rb'), params=params, headers=urlHeader())
        return upload.status_code,""
    else:
        return r.status_code, r.text

def lastUpload():        
    r = requests.get(url=LASTUPLOAD_URL, params=publicPrm(), headers=urlHeader())
    if r.status_code == 200:
        res = r.json() 
        resData = res["items"]
        return r.status_code, resData, ""
    else:
        return r.status_code, None,  r.text 

def publish(_path):        
    r = requests.put(url=PUBLISH_URL, params={"path":_path}, headers=urlHeader()) 
    err_text = ""
    if r.status_code != 200:
        err_text = r.text
    return r.status_code, err_text         

def publicDisk():
    status_code,resData, err_text = lastUpload()
    if status_code == 200:
        for data in resData:
            if "path" in data.keys() and "public_url" not in data.keys():
                status_code,err_text = publish(data["path"])
    return status_code , err_text           

def readDisk():
    r = requests.get(url=PUBLIC_URL, params=publicPrm(), headers=urlHeader())
    if r.status_code == 200:
        res = r.json() 
        resData = res["items"]
        return r.status_code, resData, ""
    else:
        return r.status_code, None, r.text