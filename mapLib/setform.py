"""
    version="1.0.0"
    Модуль для построения формы настроек

"""
import tkinter as tk
import os

class TkOrganizer(tk.Frame):
    """ Класс компонент органайзер
    """
    def __init__(self, parent=None):
        tk.Frame.__init__(self, parent)
        self.parent = parent
        self.widgets()
        
    def widgets(self):    
        def move_to(box1,box2):
            select = list(box1.curselection())
            values = box1.get(0, tk.END) 
            select.reverse()
            for _l in select:
              box2.insert(0, values[_l])  
              box1.delete(_l)
        def move_to_l(event):
            move_to(self.__rbox,self.__lbox)
        def move_to_r(event):
            move_to(self.__lbox,self.__rbox)    
        self.__mainFrame = tk.Frame(self.parent)
        self.__lbox = tk.Listbox(self.__mainFrame,selectmode=tk.EXTENDED)
        self.__lbox.pack(side=tk.LEFT)
        self.__controlbox = tk.Frame(self.__mainFrame)    
        self.__butRtoL = tk.Button(self.__controlbox,text="<<<")
        self.__butRtoL.bind('<Button-1>', move_to_l)
        self.__butRtoL.pack(fill=tk.X)
        self.__butLtoR = tk.Button(self.__controlbox,text=">>>")
        self.__butLtoR.pack(fill=tk.X)
        self.__butLtoR.bind('<Button-1>', move_to_r)
        self.__controlbox.pack(side=tk.LEFT)
        self.__rbox = tk.Listbox(self.__mainFrame,selectmode=tk.EXTENDED)
        self.__rbox.pack(side=tk.LEFT) 
        self.__mainFrame.pack(side=tk.TOP)

    def loadR(self,loadlist):
        for _l in loadlist:
            self.__rbox.insert(0,_l)

    def loadL(self,loadlist):
        for _l in loadlist:
            self.__lbox.insert(0,_l)

    @property
    def valr(self):
        return self.__rbox.get(0, tk.END)       

    @property
    def vall(self):
        return self.__lbox.get(0, tk.END)       


class SetForm:
    """ Класс Формы с настройками
        Свойства /n
        modalResult - Результат открытия формы (Ok,Cancel)
        values - Словарь со значениями
    """
    def __init__(self,name:str='form') -> None:
        self.__name = name
        self.__savefile = f'.{name}'
        self.__MapLayerfile = f'.MapLayer'
        self.__modalResult = 'Cancel'
        self.__Defvalues =  {
                "zoom": '10', 
                "fotoOn":'0', 
                "wheatherShow":'1',
                "autoOn":'1',
                "mouseposOn":'0',
                "minimapOn":'0',
                "MapLayerbox":{},
                "tzh":'0',
                "tzmin":'0'
                }
        if os.path.exists(self.__savefile):
            with open(self.__savefile, 'r') as openfile: 
                _data=openfile.read()   
                self.__values = eval(_data)
                for _val in self.__Defvalues:
                    if self.__values.get(_val) == None: 
                        self.__values[_val] = self.__Defvalues[_val] 

        else:
            self.__values = self.__Defvalues
        if os.path.exists(self.__MapLayerfile):
            with open(self.__MapLayerfile, 'r') as openfile: 
                _data=openfile.read()   
                self.__MapLayerChooseBox = eval(_data)
        else:
            self.__MapLayerChooseBox =  {}        
        # форма
        self.__rootform = tk.Tk()
        self.__rootform.title("Параметры для формирования")
        # атрибуты для контролов
        self.__ZoomVal= tk.StringVar(self.__rootform)
        self.__ZoomVal.set(self.__values["zoom"])
        # 
        self.__fotoOnVal= tk.BooleanVar(self.__rootform)
        self.__fotoOnVal.set(self.__values["fotoOn"])
        # 
        self.__wheatherShowVal= tk.BooleanVar(self.__rootform)
        self.__wheatherShowVal.set(self.__values["wheatherShow"])
        # 
        self.__autoOnVal= tk.BooleanVar(self.__rootform)
        self.__autoOnVal.set(self.__values["autoOn"])
        # 
        self.__mousePosOnVal= tk.BooleanVar(self.__rootform)
        self.__mousePosOnVal.set(self.__values["mouseposOn"])
        # 
        self.__minimapOnVal= tk.BooleanVar(self.__rootform)
        self.__minimapOnVal.set(self.__values["minimapOn"])
        # 
        self.__tzhVal= tk.StringVar(self.__rootform)
        self.__tzhVal.set(self.__values["tzh"])
        self.__tzminVal= tk.StringVar(self.__rootform)
        self.__tzminVal.set(self.__values["tzmin"])
        # контролы
        self.__labelZoom = tk.Label(text="Начальный масштаб")
        self.__SpinboxZoom = tk.Spinbox(width=3,textvariable=self.__ZoomVal, from_=1, to=18)
        # 
        self.__labelTz = tk.Label(text="Коррекция временной зоны треков (ч:м)")
        self.__SpinTzh = tk.Spinbox(width=3,textvariable=self.__tzhVal, from_= -12, to=14)
        self.__SpinTzmin = tk.Spinbox(width=3,textvariable=self.__tzminVal, from_= -45, to=45, increment=15)
        #
        self.__ChkFotoOn = tk.Checkbutton(text="Показать на карте фотографии",
                 variable=self.__fotoOnVal,onvalue=1, offvalue=0)
        self.__ChkAutoOn = tk.Checkbutton(text="Не скрывать на карте поездки на машине",
                 variable=self.__autoOnVal,onvalue=1, offvalue=0)
        self.__ChkwheatherShow = tk.Checkbutton(text="Формировать метки для погоды",
                 variable=self.__wheatherShowVal,onvalue=1, offvalue=0)
        self.__ChkmousePosOn = tk.Checkbutton(text="Показывать координаты курсора",
                 variable=self.__mousePosOnVal,onvalue=1, offvalue=0)
        self.__ChkminimapOn = tk.Checkbutton(text="Добавить миникарту",
                 variable=self.__minimapOnVal,onvalue=1, offvalue=0)
        #Органайзер слоев 
        self.__labelMapLayerbox = tk.Label(text="Дополнительные слои на карте:")
        self.__frameMapbox = tk.Frame()
        self.__orgMapLayerbox = TkOrganizer(self.__frameMapbox)
        self.__orgMapLayerbox.loadL(list(self.__values['MapLayerbox']))
        self.__orgMapLayerbox.loadR(list(filter
             (lambda x:x not in self.__values['MapLayerbox'], self.__MapLayerChooseBox)))        
        
        
        self.__buttonRun = tk.Button(text="Сформировать")
        self.__buttonCancel = tk.Button(text="Отменить")

    def show(self) -> None:
        """Размещение контролов на форме и показ формы
        """
        def runclik(event):
            #Сохранить настройки
            _MapLayerbox = {}
            if self.__MapLayerChooseBox != None:
                for _key in self.__orgMapLayerbox.vall:
                    _MapLayerbox[_key] = self.__MapLayerChooseBox[_key]  
            self.__values = {
                "zoom": self.__ZoomVal.get(), 
                "fotoOn":int(self.__fotoOnVal.get()), 
                "wheatherShow":int(self.__wheatherShowVal.get()),
                "autoOn":int(self.__autoOnVal.get()),
                "mouseposOn":int(self.__mousePosOnVal.get()),
                "minimapOn":int(self.__minimapOnVal.get()),
                "MapLayerbox":_MapLayerbox,
                "tzh":self.__tzhVal.get(),
                "tzmin":self.__tzminVal.get()
                }
            with open(self.__savefile, 'w') as savefile:    
                savefile.write(str(self.__values))
            self.__modalResult = 'Ok'
            self.__rootform.destroy()

        def cancelclik(event):
            #Действие отмены
            self.__modalResult = 'Cancel'
            self.__rootform.destroy()

        self.__labelZoom.grid(row=1, column=0, sticky = tk.W, pady=10, padx=10)
        self.__SpinboxZoom.grid(row=1, column=1, sticky = tk.W+tk.E, pady=10, padx=10)

        self.__labelTz.grid(row=2, column=0, sticky = tk.W, pady=3, padx=10)
        self.__SpinTzh.grid(row=2, column=1, sticky = tk.W+tk.E, pady=3, padx=10)
        self.__SpinTzmin.grid(row=2, column=2, sticky = tk.W, pady=3, padx=10, columnspan=2)

        self.__ChkFotoOn.grid(row=4, column=0, sticky = tk.W, pady=3, padx=10, columnspan=3)
        self.__ChkwheatherShow.grid(row=5, column=0, sticky = tk.W, pady=3, padx=10, columnspan=3)
        self.__ChkAutoOn.grid(row=6, column=0, sticky = tk.W, pady=3, padx=10, columnspan=3)
        self.__ChkmousePosOn.grid(row=7, column=0, sticky = tk.W, pady=3, padx=10, columnspan=3)
        self.__ChkminimapOn.grid(row=8, column=0, sticky = tk.W, pady=3, padx=10, columnspan=3)
        
        self.__labelMapLayerbox.grid(row=9, column=0, sticky = tk.W, pady=3, padx=10, columnspan=3)
        self.__frameMapbox.grid(row=10, column=0, pady=1, padx=10, columnspan=3)
    
        self.__buttonCancel.grid(row=20, column=1, pady=5, padx=2)
        self.__buttonRun.grid(row=20, column=2, pady=5, padx=2)
        self.__buttonRun.bind('<Button-1>', runclik)
        self.__buttonCancel.bind('<Button-1>', cancelclik)
        
        self.__rootform.resizable(False, False)
        self.__rootform.mainloop()
    
    @property
    def modalResult(self):
        return self.__modalResult
    @property
    def values(self):
        return self.__values